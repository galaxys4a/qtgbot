# PROJECT-NAME

## Create the virtual environment

Create the dev virtual environment:

```bash
./create-venv 1
```

Create the virtual environment:

```bash
./create-venv
```

## Start

```bash
./start
```
