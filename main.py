import logging
import random

from aiogram import Bot, Dispatcher, executor, types
from aiogram.contrib.fsm_storage.memory import MemoryStorage
from aiogram.dispatcher.filters.state import State, StatesGroup
from aiogram.utils.exceptions import BotBlocked

from config import API_TOKEN, USER_ID

logging.basicConfig(level=logging.DEBUG)

questions = [
    "",
    "Как называется операция получения классической информации из квантового бита?",
    "Что изменится если вместо кота Шредингера завести собаку Шредингера?",
    "Сколько классической информации можно хранить в 20 кубитах?",
    (
        "Амплитуды A и B сидели на трубе, кубит измерили, что осталось на трубе?\n\n"
        "Варианты ответов:\n"
        "1. Кубит\n"
        "2. Кубит в состоянии 0\n"
        "3. И\n"
        "4. Кубит в состоянии 0 , если A^2 > B^2, в состоянии 1, если A^2 < B^2\n"
        "5. 0 c вероятностью A*A и 1 с вероятностью B*B"
    ),
    (
        "Какое из этих событий может быть при суперпозиции квантового состояния?\n\n"
        "Варианты ответов:\n"
        "1. Кубит всегда будет в том состоянии, в котором мы хотим его увидеть\n"
        "2. Вероятность получения кубита в состоянии 0 и 1 одинакова"
    ),
    "Что такое фотон?",
    "Какой квантовый алгоритм может взломать банковские и другие шифры?",
    "Сколько сейчас есть кубит в самом мощном “универсальном” доступном квантовом компьютере?",
]

options = {
    1: [
        ("Излучение", "0"),
        ("Извлечение", "0"),
        ("Измерение", "1"),
        ("Получение", "0"),
        ("Измучение", "0"),
    ],
    2: [
        ("Ничего", "1"),
        ("Кубит станет кутритом", "0"),
        ("При состоянии ноль у собаки отвалится хвост", "0"),
        ("Перестаньте мучить бедных животных!!!", "0"),
    ],
    3: [
        ("100500", "0"),
        ("100500 в степени 100500", "0"),
        ("20", "0"),
        ("20 в степени 2", "0"),
        ("2 в степени 20", "1"),
        ("42", "0"),
        ("73", "0"),
    ],
    4: [
        ("1", "0"),
        ("2", "0"),
        ("3", "0"),
        ("4", "0"),
        ("5", "1"),
    ],
    5: [
        ("1","0"),
        ("2", "1"),
    ],
    6: [
        ("Волна","0"),
        ("Частица", "0"),
        ("Волночастица", "1"),
    ],
    7: [
            ("Алгоритм Тора", "0"),
            ("Алгоритм Шора", "1"),
            ("Алгоритм Кубита", "0"),
            ("Кулгаритм", "0"),
    ],
    8: [
            ("3", "0"),
            ("63", "0"),
            ("127", "1"),
            ("255", "0"),
            ("1023", "0"),
    ],
}


def get_keyboard(question_number: int) -> types.InlineKeyboardMarkup:
    buttons = []
    for question_option in options[question_number]:
        button = types.InlineKeyboardButton(
            text=question_option[0], callback_data=question_option[1]
        )
        buttons.append(button)
    random.shuffle(buttons)
    keyboard = types.InlineKeyboardMarkup(row_width=1)
    keyboard.add(*buttons)
    return keyboard


class QuestionsStates(StatesGroup):
    question1 = State()
    question2 = State()
    question3 = State()
    question4 = State()
    question5 = State()
    question6 = State()
    question7 = State()
    question8 = State()


bot = Bot(token=API_TOKEN)
storage = MemoryStorage()
dp = Dispatcher(bot, storage=storage)


@dp.errors_handler(exception=BotBlocked)
async def error_bot_blocked(update: types.Update, exception: BotBlocked):
    return True


@dp.message_handler(commands=["cancel"], state="*")
async def cancel_cmd(message: types.Message, state: State):
    await state.finish()
    await message.answer("Сброс")


@dp.message_handler(state=QuestionsStates.all_states)
async def handle_question_message(message: types.Message):
    await message.answer("Ответьте, пожалуйста, используя кнопки!")


@dp.message_handler(commands=["start"])
async def start(message: types.Message, state: State):
    await bot.send_message(USER_ID, f"{message.from_user}\n@{message.from_user.username}")
    user_id = message.from_id
    with open("user_ids.txt", "r") as f:
        str_user_ids = f.read()
    user_ids = [int(str_user_id) for str_user_id in str_user_ids.split("\n")[:-1]]
    if user_id in user_ids:
        await message.answer("Вы уже прошли тестирование!")
        return
    await QuestionsStates.question1.set()
    await message.answer(questions[1], reply_markup=get_keyboard(1))


@dp.callback_query_handler(state=QuestionsStates.question1)
async def handle_question1(call: types.CallbackQuery, state: State):
    await state.update_data(points=int(call.data))
    await QuestionsStates.question2.set()
    await call.message.delete()
    await call.message.answer(questions[2], reply_markup=get_keyboard(2))
    await call.answer()


@dp.callback_query_handler(state=QuestionsStates.question2)
async def handle_question2(call: types.CallbackQuery, state: State):
    data = await state.get_data()
    points = data["points"] + int(call.data)
    await state.update_data(points=points)
    await QuestionsStates.question3.set()
    await call.message.delete()
    await call.message.answer(questions[3], reply_markup=get_keyboard(3))
    await call.answer()


@dp.callback_query_handler(state=QuestionsStates.question3)
async def handle_question3(call: types.CallbackQuery, state: State):
    data = await state.get_data()
    points = data["points"] + int(call.data)
    await state.update_data(points=points)
    await QuestionsStates.question4.set()
    await call.message.delete()
    await call.message.answer(questions[4], reply_markup=get_keyboard(4))
    await call.answer()


@dp.callback_query_handler(state=QuestionsStates.question4)
async def handle_question4(call: types.CallbackQuery, state: State):
    data = await state.get_data()
    points = data["points"] + int(call.data)
    await state.update_data(points=points)
    await QuestionsStates.question5.set()
    await call.message.delete()
    await call.message.answer(questions[5], reply_markup=get_keyboard(5))
    await call.answer()

@dp.callback_query_handler(state=QuestionsStates.question5)
async def handle_question5(call: types.CallbackQuery, state: State):
    data = await state.get_data()
    points = data["points"] + int(call.data)
    await state.update_data(points=points)
    await QuestionsStates.question6.set()
    await call.message.delete()
    await call.message.answer(questions[6], reply_markup=get_keyboard(6))
    await call.answer()

@dp.callback_query_handler(state=QuestionsStates.question6)
async def handle_question6(call: types.CallbackQuery, state: State):
    data = await state.get_data()
    points = data["points"] + int(call.data)
    await state.update_data(points=points)
    await QuestionsStates.question7.set()
    await call.message.delete()
    await call.message.answer(questions[7], reply_markup=get_keyboard(7))
    await call.answer()

@dp.callback_query_handler(state=QuestionsStates.question7)
async def handle_question7(call: types.CallbackQuery, state: State):
    data = await state.get_data()
    points = data["points"] + int(call.data)
    await state.update_data(points=points)
    await QuestionsStates.question8.set()
    await call.message.delete()
    await call.message.answer(questions[8], reply_markup=get_keyboard(8))
    await call.answer()

@dp.callback_query_handler(state=QuestionsStates.question8)
async def handle_question8(call: types.CallbackQuery, state: State):
    data = await state.get_data()
    points = data["points"] + int(call.data)
    await state.update_data(points=points)
    await state.finish()
    await call.message.delete()
    photo = open("q.jpg", "rb")
    await bot.send_photo(call.from_user.id, photo)
    await call.message.answer(
        f"Поздравляем, Вы прошли тест😎! Количество правильных ответов: {points}\n\nПриглашаем Вас в сообщество QRussia: https://t.me/+VPXHX6q87aiCHbVD"
    )
    await call.answer()
    user_id = call.from_user.id
    with open("user_ids.txt", "a") as f:
        f.write(str(user_id) + "\n")


async def setup_bot_commands(dp: Dispatcher):
    bot_commands = [
        types.BotCommand(command="/start", description="Начать тестирование"),
        types.BotCommand(command="/cancel", description="Сброс"),
    ]
    await dp.bot.set_my_commands(bot_commands)


if __name__ == "__main__":
    executor.start_polling(dp, skip_updates=True, on_startup=setup_bot_commands)
